<!-- HEADER -->
<header>
			
    <!-- MENU BLOCK -->
    <div class="menu_block">
    
        <!-- CONTAINER -->
        <div class="container clearfix" style="position:relative;">

            <!--carrinho-compras-->
            <div id="carrinho-compras">
                <a href="javascript:void(0);">
                    <img src="<?php bloginfo( 'template_url' ); ?>/images/my-cart.png">
                    <span id="qtd"></span>
                </a>
            </div>
            <div id="carrinho-compras-itens">
                <ul></ul>
                <a href="<?php echo esc_url( home_url( '/carrinho' ) ); ?>" id="goCart">ver carrinho</a>
            </div>
            <script type="text/javascript">
                var cart = JSON.parse(sessionStorage.getItem("carrinho"));
                var valorTotal = 0;
                if(!cart || cart.length === 0) {
                    $("#carrinho-compras").hide();
                } else {
                    $("#carrinho-compras").show();
                    $("#qtd").html(cart.length);
                    $.each( cart, function( key, value ) {
                        var valorItem = parseFloat(value.itemAmount.substr(3)) * parseInt(value.itemQuantity);
                        valorTotal += valorItem;
                        $("#carrinho-compras-itens ul").append('<li><span>'+ value.itemQuantity +'x  </span> '+ value.itemDescription +' <a onclick="removeItem('+ key +')" class="remove">x</a></li>');
                    });
                }

                if(valorTotal > 0) {
                    $("#carrinho-compras-itens ul").append('<li><span>TOTAL: R$ '+ valorTotal +'</span></li>');
                }

                function removeItem(index){
                    var mycart = JSON.parse(sessionStorage.getItem("carrinho"));
                    mycart.splice(index, 1);
                    sessionStorage.setItem("carrinho", JSON.stringify(mycart));
			        location.reload();
                }
            </script>
            <!--/carrinho-compras-->
            
            <!-- LOGO -->
            <div class="logo">
                <a href="index.html" >
                    <img src="<?php bloginfo( 'template_url' ); ?>/images/logo.png" width="100">
                </a>
            </div><!-- //LOGO -->
            
            <!-- MENU -->
            <div class="menu">
                <nav class="navmenu center">
                    <ul>
                        <li class="first active scroll_btn"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"" >Home</a></li>
                        <li class="scroll_btn"><a href="<?php echo esc_url( home_url( '/sobre' ) ); ?>">Sobre</a></li>
                        <li class="scroll_btn"><a href="<?php echo esc_url( home_url( '/lookbook' ) ); ?>" >Lookbook</a></li>
                        <li class="sub-menu">
                            <a href="javascript:void(0);" >Produtos</a>
                            <ul>
                                <li><a href="<?php echo esc_url( home_url( '/produtos?group=anel' ) ); ?>">Anel</a></li>
                                <li><a href="<?php echo esc_url( home_url( '/produtos?group=brinco' ) ); ?>">Brinco</a></li>
                                <li><a href="<?php echo esc_url( home_url( '/produtos?group=chocker' ) ); ?>">Chocker</a></li>
                                <li><a href="<?php echo esc_url( home_url( '/produtos?group=colar' ) ); ?>">Colar</a></li>
                                <li><a href="<?php echo esc_url( home_url( '/produtos?group=pulseira' ) ); ?>">Pulseira</a></li>
                                <li><a href="<?php echo esc_url( home_url( '/produtos?group=bracelet' ) ); ?>">Bracelet</a></li>
                            </ul>
                        </li>
                        <li class="scroll_btn last"><a href="<?php echo esc_url( home_url( '/contato' ) ); ?>">Contato</a></li>
                    </ul>
                </nav>
            </div><!-- //MENU -->
        </div><!-- //MENU BLOCK -->
    </div><!-- //CONTAINER -->
</header><!-- //HEADER -->