<?php
get_header();
?>

	<?php
		$parent_title = get_the_title( $post->post_parent );
		$content = get_the_content( $post->post_parent );
	?>

	<!-- PAGE -->
	<div id="page">
	
		<!-- ADCIONANDO MENU -->
		<?php
			get_template_part( 'template-parts/menu', 'menu' );
		?>
		<!-- / ADCIONANDO MENU -->
		
		<!-- ABOUT -->
		<section id="about">
			
			<!-- SERVICES -->
			<div class="services_block padbot40" data-appear-top-offset="-200" data-animated="fadeInUp">
				
				<!-- CONTAINER -->
				<div class="lookbook">
				
					<div class="masonry" style="height: 5670.55px;">
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img15.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img2.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img3.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img4.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><video controls class="masonry-img"><source src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/video1.mp4" type="video/mp4"></video></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img5.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img6.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img7.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img8.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><video controls class="masonry-img"><source src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/video2.mp4" type="video/mp4"></video></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img9.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img10.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img11.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img12.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><video controls class="masonry-img"><source src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/video3.mp4" type="video/mp4"></video></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img13.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img14.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img1.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img16.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img17.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><video controls class="masonry-img"><source src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/video4.mp4" type="video/mp4"></video></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img18.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img19.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img20.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img21.jpeg" class="masonry-img"></figure>
						<figure class="masonry-brick"><img src="<?php bloginfo( 'template_url' ); ?>/images/lookbook/img15.jpeg" class="masonry-img"></figure>
					</div>

				<script src="<?php bloginfo( 'template_url' ); ?>/js/imagesloaded.pkgd.min.js"></script>
				<script>
					function masonry(grid, gridCell, gridGutter, dGridCol, tGridCol, mGridCol) {
					var g = document.querySelector(grid),
						gc = document.querySelectorAll(gridCell),
						gcLength = gc.length,
						gHeight = 0,
						i;

					for (i = 0; i < gcLength; ++i) {
						gHeight += gc[i].offsetHeight + parseInt(gridGutter);
					}

					if (window.screen.width >= 1024)
						g.style.height = gHeight / dGridCol + gHeight / (gcLength + 1) + "px";
					else if (window.screen.width < 1024 && window.screen.width >= 768)
						g.style.height = gHeight / tGridCol + gHeight / (gcLength + 1) + "px";
					else
						g.style.height = gHeight / mGridCol + gHeight / (gcLength + 1) + "px";
					}

					var masonryGrid = document.querySelector('.masonry');

					["resize", "load"].forEach(function (event) {
					masonryGrid.style.display = "none";
					window.addEventListener(event, function () {
						imagesLoaded(document.querySelector('.masonry'), function () {
						masonryGrid.style.display = "flex";
						masonry(".masonry", ".masonry-brick", 8, 3, 2, 1);
						});
					}, false);
					});
				</script>

				</div><!-- //CONTAINER -->
			</div><!-- //SERVICES -->
			
		</section><!-- //ABOUT -->
		
	</div><!-- //PAGE -->

<?php
get_footer();