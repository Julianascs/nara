<?php
get_header();
?>
	<?php
		$parent_title = get_the_title( $post->post_parent );
	?>

	<script type="text/javascript">
		function addCart() {
			var nomeProduto = $("#name").text();
			var precoProduto = $("#price").text();
			var qtdProdutos = $("#amount").val()  === "Quantidade" ? 1 : $("#amount").val();
			var urlImg = $("#imgProd").attr('src');
			var tamanho = $("#tamanho").val();
			console.log(tamanho);
			var cor = $("#cor").val();
			console.log(cor);
			var frase = $("#frase").val();
			console.log(frase);
			var opcaoFrase = $("input[name='opcaoFrase']").val();
			console.log(opcaoFrase);
			var opcaoCoracao = $("input[name='opcaoCoracao']").val();
			console.log(opcaoCoracao);
			var estampa = $("#lencoslist li.active").find("img").attr('src');
			console.log(estampa);
			var pedra = $("#cores_pedra_list li.active").find("img").attr('src');
			console.log(pedra);
			var corFio = $("#cores_fio_list li.active").attr('value');
			console.log(corFio);

			var meuItem = {
				"itemDescription": nomeProduto,
				"itemAmount": precoProduto,
				"itemQuantity": qtdProdutos,
				"urlProduto": urlImg
			};
			if(tamanho !== undefined) {meuItem.tamanho = tamanho};
			if(cor !== undefined) {meuItem.cor = cor};
			if(frase !== undefined) {meuItem.frase = frase};
			if(opcaoFrase !== undefined) {meuItem.opcaoFrase = opcaoFrase};
			if(opcaoCoracao !== undefined) {meuItem.opcaoCoracao = opcaoCoracao};
			if(estampa !== undefined) {meuItem.estampa = estampa};
			if(pedra !== undefined) {meuItem.pedra = pedra};
			if(corFio !== undefined) {meuItem.corFio = corFio};

			var meuCarrinho = JSON.parse(sessionStorage.getItem("carrinho"));
			if(meuCarrinho === null) meuCarrinho = [];
			meuCarrinho.push(meuItem)
			sessionStorage.setItem("carrinho", JSON.stringify(meuCarrinho));
			location.reload();
		}
	</script>

	<!-- PAGE -->
	<div id="page">

		<!-- ADCIONANDO MENU -->
		<?php
			get_template_part( 'template-parts/menu', 'menu' );
		?>
		<!-- / ADCIONANDO MENU -->

		<!-- BREADCRUMBS -->
		<section class="breadcrumbs_block clearfix parallax breadcrumbs_small">
			<div class="container center">
				<h2><?php print_r($parent_title) ?></h2>
			</div>
		</section><!-- //BREADCRUMBS -->

		<!-- ABOUT -->
		<section id="about">

			<!-- SERVICES -->
			<div class="services_block" data-appear-top-offset="-200" data-animated="fadeInUp">

				<!-- CONTAINER -->
				<div class="container">
					
					<!-- ROW -->
					<div class="row">

					<?php 
						$id = $_GET['item'];
						$post = get_post( $id );
					?>
						
						<!-- SIDEBAR -->
						<div class="sidebar col-lg-4 col-md-4 pull-right">
							<!-- TEXT WIDGET -->
							<div class="sidepanel widget_text">
								<div class="single_portfolio_post_title" id="name"><?php the_title(); ?></div>
								<p class="product-price" id="price">R$ <?php the_field('preco'); ?></p>
								<input type="text" name="quantidade" value="Quantidade" onFocus="if (this.value == 'Quantidade') this.value = '';" onBlur="if (this.value == '') this.value = 'Quantidade';"  id="amount" />
								<?php if( get_field('tamanho_produto') ) { 
									$tamanhos = strval(get_field('tamanho_produto'));
									$list = explode(',', $tamanhos);
								?>
								<select name="tamanho" id="tamanho">
									<option value="">Tamanho</option>
									<?php foreach($list as $value){?>
										<option value="<?php echo $value?>"><?php echo $value?></option>
									<?php }?>
								</select>
								<?php } ?>
								<?php if( get_field('cor_produto') ) { 
									$cores = strval(get_field('cor_produto'));
									$listCores = explode(',', $cores);
								?>
								<select name="cor" id="cor">
									<option value="">Cor</option>
									<?php foreach($listCores as $valueC){?>
										<option value="<?php echo $valueC?>"><?php echo $valueC?></option>
									<?php }?>
								</select>
								<?php } ?>
								<?php if( get_field('has_frase_produto') ) { 
									$qtdLimite = get_field('maxLength_frase')
								?>
									<input type="text" name="frase" id="frase" value="Frase" onFocus="if (this.value == 'Frase') this.value = '';" onBlur="if (this.value == '') this.value = 'Frase';" maxlength="<?php echo $qtdLimite?>"/>
								<?php } ?>
								<?php if( get_field('has_fonte_produto') ) { ?>
									<div id="escolha_coracao">
										<div>
											<input type="radio" id="manuscrita" name="opcaoFrase" value="manuscrita">
											<label for="manuscrita">Fonte Manuscrita</label>
										</div>

										<div>
											<input type="radio" id="forma" name="opcaoFrase" value="forma">
											<label for="forma">Fonte de Forma</label>
										</div>
									</div>
								<?php } ?>
								<?php if( get_field('has_escolha_estampas') ) { ?>
									<div id="lencos">
										<label>Escolha uma estampa</label>
										<ul id="lencoslist">
											<li><a href="javascript:void;"><img src="<?php bloginfo( 'template_url' ); ?>/images/estampas/1.jpg" width="100"></a></li>
											<li><a href="javascript:void;"><img src="<?php bloginfo( 'template_url' ); ?>/images/estampas/2.jpg" width="100"></a></li>
											<li><a href="javascript:void;"><img src="<?php bloginfo( 'template_url' ); ?>/images/estampas/3.jpg" width="100"></a></li>
											<li><a href="javascript:void;"><img src="<?php bloginfo( 'template_url' ); ?>/images/estampas/4.jpg" width="100"></a></li>
											<li><a href="javascript:void;"><img src="<?php bloginfo( 'template_url' ); ?>/images/estampas/5.jpg" width="100"></a></li>
											<li><a href="javascript:void;"><img src="<?php bloginfo( 'template_url' ); ?>/images/estampas/6.jpg" width="100"></a></li>
										</ul>
									</div>
								<?php } ?>
								<?php if( get_field('has_escolha_cor_fio') ) { ?>
									<div id="cores_fio">
										<label>Escolha uma cor de Fio</label>
										<ul id="cores_fio_list">
											<li value="rosa" id="cor1"></li>
											<li value="verde" id="cor2"></li>
											<li value="marrom" id="cor3"></li>
											<li value="amarelo" id="cor4"></li>
											<li value="azul" id="cor5"></li>
										</ul>
									</div>
								<?php } ?>
								<?php if( get_field('imagem_pedra_1') ) { ?>
									<div id="cores_pedra">
									<label>Escolha uma cor de pedra</label>
										<ul id="cores_pedra_list">
											<li><a href="javascript:void;"><img src="<?php the_field('imagem_pedra_1'); ?>"></a></li>
											<?php if( get_field('imagem_pedra_2') ) { ?>
												<li><a href="javascript:void;"><img src="<?php the_field('imagem_pedra_2'); ?>"></a></li>
											<?php } ?>
											<?php if( get_field('imagem_pedra_3') ) { ?>
												<li><a href="javascript:void;"><img src="<?php the_field('imagem_pedra_3'); ?>"></a></li>
											<?php } ?>
											<?php if( get_field('imagem_pedra_4') ) { ?>
												<li><a href="javascript:void;"><img src="<?php the_field('imagem_pedra_4'); ?>"></a></li>
											<?php } ?>
											<?php if( get_field('imagem_pedra_5') ) { ?>
												<li><a href="javascript:void;"><img src="<?php the_field('imagem_pedra_5'); ?>"></a></li>
											<?php } ?>
										</ul>
									</div>
								<?php } ?>
								<?php if( get_field('has_escolha_coracao') ) { ?>
									<div id="escolha_coracao">
										<div>
											<input type="radio" id="sim" name="opcaoCoracao" value="sim">
											<label for="sim">Com coração</label>
										</div>

										<div>
											<input type="radio" id="nao" name="opcaoCoracao" value="nao">
											<label for="nao">Sem Coraçao</label>
										</div>
									</div>
								<?php } ?>
							</div><!-- //TEXT WIDGET -->
							
							<hr>
							
							<!-- INFO WIDGET -->
							<div class="sidepanel widget_info">
								<ul class="work_info">
									<li><?php echo $post->post_content; ?></li>
									<?php if(in_category("anel")) { ?>
										<li><b>Tabela de Medidas:</b> <a href="<?php bloginfo( 'template_url' ); ?>/images/tabela_medidas_anel.jpg" target="_blank">Clique aqui</a></li>
									<?php } ?>
								</ul>
							</div><!-- //INFO WIDGET -->

							<input class="contact_btn" type="submit" value="Adicionar ao carrinho" style="width:100%;" onclick="addCart()" />
						</div><!-- //SIDEBAR -->
						
						
						<!-- PORTFOLIO BLOCK -->
						<div class="portfolio_block col-lg-8 col-md-8 pull-left">
							
							<!-- SINGLE PORTFOLIO POST -->
							<div class="single_portfolio_post clearfix" data-animated="fadeInUp">
								
								<!-- PORTFOLIO SLIDER -->
								<div class="flexslider portfolio_single_slider">
									<ul class="slides">
									
										<li><img src="<?php the_field('fotos_produto_1'); ?>" alt="" id="imgProd" /></li>
										<?php if( get_field('fotos_produto_2') ) { ?>
										<li><img src="<?php the_field('fotos_produto_2'); ?>" alt="" /></li>
										<?php } ?>
										<?php if( get_field('fotos_produto_3') ) { ?>
										<li><img src="<?php the_field('fotos_produto_3'); ?>" alt="" /></li>
										<?php } ?>
									</ul>
								</div><!-- //PORTFOLIO SLIDER -->
							</div><!-- //SINGLE PORTFOLIO POST -->
						</div><!-- //PORTFOLIO BLOCK -->
					</div><!-- //ROW -->
				</div><!-- //CONTAINER -->

			</div><!-- //SERVICES -->
		</section><!-- //ABOUT -->

		<!-- NEWS -->
		<section id="news">

			<!-- CONTAINER -->
			<div class="container">
				<h2><b>Opinião</b> de Clientes</h2>

				<!-- TESTIMONIALS -->
				<div class="testimonials" data-appear-top-offset="-200" data-animated="fadeInUp">

					<!-- TESTIMONIALS SLIDER -->
					<div class="owl-demo owl-carousel testim_slider">

						<!-- TESTIMONIAL1 -->
						<div class="item">
							<div class="testim_content">“Poderia dizer que a Nara tem um talento único. Consegue fazer peças elegantes,
								modernas e quase que exclusivas. Acompanho o trabalho dela desde que ela começou e me apaixono em cada
								lançamento ♥.”</div>
							<div class="testim_author">— Roseanne Alves</div>
						</div><!-- TESTIMONIAL1 -->

						<!-- TESTIMONIAL2 -->
						<div class="item">
							<div class="testim_content">“Nara é uma artista da afetividade, além de ser extremamente criativa, talentosa e
								sensível , usa suas obras , criações, para nos podermos “ gravar “ nossos sentires para sempre !”</div>
							<div class="testim_author">— Pilar Brown</div>
						</div><!-- TESTIMONIAL2 -->

						<!-- TESTIMONIAL3 -->
						<div class="item">
							<div class="testim_content">“O trabalho da Nara é tão sensível e precioso que isso está potencialmente presente
								nas peças dela. Me emociona. Tem 3 anos que eu uso meus anéis todos os dias. Sinto eles sendo tão parte de
								mim, que quando estou sem eles é como se eu não estivesse completa.”</div>
							<div class="testim_author">— Camilla Molica</div>
						</div><!-- TESTIMONIAL3 -->

					</div><!-- TESTIMONIALS SLIDER -->
				</div><!-- //TESTIMONIALS -->


				<div class="row">
					<div class="col-md-6 col-centered">
						<ul class="social">
							<li>SIGA-NOS:</li>
							<li><a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a></li>
							<li><a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a></li>
							<li><a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a></li>
						</ul>
					</div>
				</div>
			</div><!-- //CONTAINER -->
		</section><!-- //NEWS -->
	</div><!-- //PAGE -->

	<script type="text/javascript">
		$(document).ready(function(){
			$("#lencoslist li").on("click", function() {
				$("#lencoslist li").removeClass("active");
				$(this).addClass("active");
			});
			$("#cores_fio_list li").on("click", function() {
				$("#cores_fio_list li").removeClass("active");
				$(this).addClass("active");
			});
			$("#cores_pedra_list li").on("click", function() {
				$("#cores_pedra_list li").removeClass("active");
				$(this).addClass("active");
			});
		});
	</script>

<?php
get_footer();