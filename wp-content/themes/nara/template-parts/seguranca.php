<?php
get_header();
?>

	<?php
		$parent_title = get_the_title( $post->post_parent );
		$content = get_the_content( $post->post_parent );
	?>

	<!-- PAGE -->
	<div id="page">
	
		<!-- ADCIONANDO MENU -->
		<?php
			get_template_part( 'template-parts/menu', 'menu' );
		?>
		<!-- / ADCIONANDO MENU -->
		
		<!-- ABOUT -->
		<section id="about">
			
			<!-- SERVICES -->
			<div class="services_block padbot40" data-appear-top-offset="-200" data-animated="fadeInUp">
				
				<!-- CONTAINER -->
				<div class="container">
				
					<!-- ROW -->
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-ss-12 margbot30 col-centered">
							<a class="services_item" href="javascript:void(0);" >
								<p><b><?php print_r($parent_title) ?></b></p>
								<?php print_r($content) ?>
							</a>
						</div>
					</div><!-- //ROW -->

				</div><!-- //CONTAINER -->
			</div><!-- //SERVICES -->
			
		</section><!-- //ABOUT -->
		
	</div><!-- //PAGE -->

<?php
get_footer();