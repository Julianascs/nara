<?php
get_header();
?>
	<?php
		$parent_title = get_the_title( $post->post_parent );
	?>

	<!-- PAGE -->
	<div id="page">

		<!-- ADCIONANDO MENU -->
		<?php
			get_template_part( 'template-parts/menu', 'menu' );
		?>
		<!-- / ADCIONANDO MENU -->

		<!-- BREADCRUMBS -->
		<section class="breadcrumbs_block clearfix parallax">
			<div class="container center">
				<h2><?php print_r($parent_title) ?></h2>
				<p><?php the_field('subtitulo') ?></p>
			</div>
		</section><!-- //BREADCRUMBS -->

		<!-- ABOUT -->
		<section id="about">

			<!-- SERVICES -->
			<div class="services_block padbot40" data-appear-top-offset="-200" data-animated="fadeInUp">

				<!-- CONTAINER -->
				<div class="container">

					<!-- ROW -->
					<div class="row sobre">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-ss-12 col-centered">
							<a class="services_item" href="javascript:void(0);">
								<div class="row">
									<div class="col-md-4 fotoNara"><img src="<?php bloginfo( 'template_url' ); ?>/images/nara.jpg"></div>
									<div class="col-md-8">
										Acredito que para falar sobre a marca eu tenha que falar um pouco sobre mim antes. Até porque a marca leva meu nome, Nara, e, vamos dizer, 
										que meu “sobrenome” também. Enfim, leva tudo mim, dedicação total.
										<br><br>
										Sou de Recife -PE, formada em Jornalismo e quase formada em Designer de Moda. Decidi parar faltando um ano de curso para ir morar no 
										Rio de Janeiro, em 2009, sozinha e trabalhar na loja Farm Ipanema. E lá fiz um tanto de curso na área da moda: Jornalismo de Moda, 
										Produção de Moda, Vitrinista, Consultoria de Imagem...
										<br><br>
										Em 2011, após ter saído do trabalho (estava trabalhando numa coluna que falava sobre celebridades “Retratos da Vida”, do Jornal EXTRA, 
										do grupo Globo), decidi que iria fazer um blog de moda, chamado “Sem Regras”. De quebra, em um jantar com amigas da amiga, saímos de 
										lá com a certeza que iriamos fazer Bijoux para vender.
										<br><br>
										Fomos comprar o material e fui testando se eu tinha habilidade ou não. Gostei, mas esse trabalho ainda estava em segundo plano. 
										Teve blog, teve volta pra Recife, teve lançamento de uma marca de T-shirt, a Six Tees (@usesixtees), que ainda existe. Até que eu 
										resolver colocar minha paixão pela joalheria artesanal em prática. Sempre fui “a louca” do personalizado e já mandada fazer minhas 
										peças em prata por aí. Então, fiz um curso de Joalheria Artesanal, e mãos à obra, literalmente.
										<br><br>
										Depois de 3 anos vendendo pelo Instagram, e com um plano de sempre fazer o site. Agora chegou a vez dele, e como nessa vida tudo 
										tem sua hora certa, acredito não ter hora mais certa que essa.
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<br><br>
										Aqui no site, vocês irão ficar com um gostinho de quero mais. Lembrar que sempre vai ter uma história pra contar em formato de joia 
										(com frases, palavras, lugares, pessoas... alguma coisa que marcaram vocês) e ainda peças básicas para fazer misturas Mara!!! no seu 
										look, estilo, personalidade. 
									</div>
								</div>
							</a>
						</div>
					</div><!-- //ROW -->

				</div><!-- //CONTAINER -->
			</div><!-- //SERVICES -->
		</section><!-- //ABOUT -->

		<!-- NEWS -->
		<section id="news">

			<!-- CONTAINER -->
			<div class="container">
				<h2><b>Opinião</b> de Clientes</h2>

				<!-- TESTIMONIALS -->
				<div class="testimonials" data-appear-top-offset="-200" data-animated="fadeInUp">

					<!-- TESTIMONIALS SLIDER -->
					<div class="owl-demo owl-carousel testim_slider">

						<!-- TESTIMONIAL1 -->
						<div class="item">
							<div class="testim_content">“Poderia dizer que a Nara tem um talento único. Consegue fazer peças elegantes,
								modernas e quase que exclusivas. Acompanho o trabalho dela desde que ela começou e me apaixono em cada
								lançamento ♥.”</div>
							<div class="testim_author">— Roseanne Alves</div>
						</div><!-- TESTIMONIAL1 -->

						<!-- TESTIMONIAL2 -->
						<div class="item">
							<div class="testim_content">“Nara é uma artista da afetividade, além de ser extremamente criativa, talentosa e
								sensível , usa suas obras , criações, para nos podermos “ gravar “ nossos sentires para sempre !”</div>
							<div class="testim_author">— Pilar Brown</div>
						</div><!-- TESTIMONIAL2 -->

						<!-- TESTIMONIAL3 -->
						<div class="item">
							<div class="testim_content">“O trabalho da Nara é tão sensível e precioso que isso está potencialmente presente
								nas peças dela. Me emociona. Tem 3 anos que eu uso meus anéis todos os dias. Sinto eles sendo tão parte de
								mim, que quando estou sem eles é como se eu não estivesse completa.”</div>
							<div class="testim_author">— Camilla Molica</div>
						</div><!-- TESTIMONIAL3 -->

					</div><!-- TESTIMONIALS SLIDER -->
				</div><!-- //TESTIMONIALS -->


				<div class="row">
					<div class="col-md-6 col-centered">
						<ul class="social">
							<li>SIGA-NOS:</li>
							<li><a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a></li>
							<li><a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a></li>
							<li><a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a></li>
						</ul>
					</div>
				</div>
			</div><!-- //CONTAINER -->
		</section><!-- //NEWS -->
	</div><!-- //PAGE -->

<?php
get_footer();