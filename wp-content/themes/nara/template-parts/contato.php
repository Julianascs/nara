<?php
get_header();
?>

		<?php
			$parent_title = get_the_title( $post->post_parent );
		?>

		<!-- PAGE -->
		<div id="page">

		<!-- ADCIONANDO MENU -->
		<?php
			get_template_part( 'template-parts/menu', 'menu' );
		?>
		<!-- / ADCIONANDO MENU -->


		<!-- BREADCRUMBS -->
		<section class="breadcrumbs_block clearfix parallax">
			<div class="container center">
				<h2><?php print_r($parent_title) ?></h2>
				<p class="break_text"><?php the_field('subtitulo') ?></p>
			</div>
		</section><!-- //BREADCRUMBS -->

		<!-- ABOUT -->
		<section id="about">

			<!-- SERVICES -->
			<div class="services_block padbot40" data-appear-top-offset="-200" data-animated="fadeInUp">

				<!-- CONTAINER -->
				<div class="container">

					<!-- ROW -->
					<div class="row sobre">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-ss-12 col-centered">
							<a class="services_item" href="javascript:void(0);">
								<div class="row">
									<div class="col-md-6 col-centered">
										<form id="contact-form-face" class="clearfix" action="<?php bloginfo( 'template_url' ); ?>/contatoEnviar.php" method="post">
											<input type="text" name="nome" value="Nome" onFocus="if (this.value == 'Nome') this.value = '';" onBlur="if (this.value == '') this.value = 'Nome';" />
											<input type="text" name="telefone" value="Telefone" onFocus="if (this.value == 'Telefone') this.value = '';" onBlur="if (this.value == '') this.value = 'Telefone';" />
											<input type="text" name="email" value="Email" onFocus="if (this.value == 'Email') this.value = '';" onBlur="if (this.value == '') this.value = 'Email';" />
											<textarea name="mensagem" onFocus="if (this.value == 'Mensagem') this.value = '';" onBlur="if (this.value == '') this.value = 'Mensagem';">Mensagem</textarea>
											<input class="contact_btn" type="submit" value="Enviar mensagem" style="float: right;" />
										</form>
									</div>
								</div>
							</a>
						</div>
					</div><!-- //ROW -->

				</div><!-- //CONTAINER -->
			</div><!-- //SERVICES -->
		</section><!-- //ABOUT -->

		<!-- NEWS -->
		<section id="news">

			<!-- CONTAINER -->
			<div class="container">
				<h2><b>Opinião</b> de Clientes</h2>

				<!-- TESTIMONIALS -->
				<div class="testimonials" data-appear-top-offset="-200" data-animated="fadeInUp">

					<!-- TESTIMONIALS SLIDER -->
					<div class="owl-demo owl-carousel testim_slider">

						<!-- TESTIMONIAL1 -->
						<div class="item">
							<div class="testim_content">“Poderia dizer que a Nara tem um talento único. Consegue fazer peças elegantes,
								modernas e quase que exclusivas. Acompanho o trabalho dela desde que ela começou e me apaixono em cada
								lançamento ♥.”</div>
							<div class="testim_author">— Roseanne Alves</div>
						</div><!-- TESTIMONIAL1 -->

						<!-- TESTIMONIAL2 -->
						<div class="item">
							<div class="testim_content">“Nara é uma artista da afetividade, além de ser extremamente criativa, talentosa e
								sensível , usa suas obras , criações, para nos podermos “ gravar “ nossos sentires para sempre !”</div>
							<div class="testim_author">— Pilar Brown</div>
						</div><!-- TESTIMONIAL2 -->

						<!-- TESTIMONIAL3 -->
						<div class="item">
							<div class="testim_content">“O trabalho da Nara é tão sensível e precioso que isso está potencialmente presente
								nas peças dela. Me emociona. Tem 3 anos que eu uso meus anéis todos os dias. Sinto eles sendo tão parte de
								mim, que quando estou sem eles é como se eu não estivesse completa.”</div>
							<div class="testim_author">— Camilla Molica</div>
						</div><!-- TESTIMONIAL3 -->

					</div><!-- TESTIMONIALS SLIDER -->
				</div><!-- //TESTIMONIALS -->


				<div class="row">
					<div class="col-md-6 col-centered">
						<ul class="social">
							<li>SIGA-NOS:</li>
							<li><a href="javascript:void(0);"><i class="fa fa-instagram"></i></a></li>
							<li><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
							<li><a href="javascript:void(0);"><i class="fa fa-pinterest-square"></i></a></li>
						</ul>
					</div>
				</div>
			</div><!-- //CONTAINER -->
		</section><!-- //NEWS -->
		</div><!-- //PAGE -->

<?php
get_footer();