<?php
get_header();
?>

	<?php
		$parent_title = get_the_title( $post->post_parent );
		$content = get_the_content( $post->post_parent );
	?>

	<!-- PAGE -->
	<div id="page">
	
		<!-- ADCIONANDO MENU -->
		<?php
			get_template_part( 'template-parts/menu', 'menu' );
		?>
		<!-- / ADCIONANDO MENU -->
		
		<!-- ABOUT -->
		<section id="about">
			
			<!-- SERVICES -->
			<div class="services_block padbot40" data-appear-top-offset="-200" data-animated="fadeInUp">
				
				<!-- CONTAINER -->
				<div class="container">
				
					<!-- ROW -->
					<div class="row">
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-ss-12 margbot30 col-centered carrinho">

						<h2><?php the_field('subtitulo') ?></h2>

						<ul id="itensCarrinho"></ul>

						<script type="text/javascript">
							var cart = JSON.parse(sessionStorage.getItem("carrinho"));
							var valorTotal = 0;
							if(!cart || cart.length === 0) {
								window.location.href = "http://www.narajewelry.com.br/";
							} else {
								$.each( cart, function( key, value ) {
									var valorItem = parseFloat(value.itemAmount.substr(3)) * parseInt(value.itemQuantity);
									valorTotal += valorItem;
									$("#itensCarrinho").append('<li><img src="'+ value.urlProduto +'"> <input type="text" disabled value="'+ value.itemQuantity +'"><span> x </span> <p>'+ value.itemDescription +' - '+ value.itemAmount +'</p> <a onclick="removeItem('+ key +')" class="remove">x</a></li>')
								});
							}

							if(valorTotal > 0) {
								$("#itensCarrinho").append('<li id="valorTotal">VALOR TOTAL: <span>R$ '+ valorTotal +'</span></li>');
							}

							function removeItem(index){
								var mycart = JSON.parse(sessionStorage.getItem("carrinho"));
								mycart.splice(index, 1);
								sessionStorage.setItem("carrinho", JSON.stringify(mycart));
								location.reload();
							}

							function pagSeguro(){
								var curl = "https://ws.pagseguro.uol.com.br/v2/checkout?"
								+"email=naraccorrea@gmail.com"
								+"&token=B95BDDDEE8F0495184BC9F319E8DB296"
								var dataPost = "&currency=BRL";
								var stringItems = "";
								$.each( cart, function( key, value ) {
									var i = key+1;
									var item = "&itemId"+i+"=000"+i+"&itemDescription"+i+"="+value.itemDescription+"&itemAmount"+i+"="+(value.itemAmount.substr(3)).replace(/,/g, '.')+"&itemQuantity"+i+"="+value.itemQuantity+"&itemWeight"+i+"=1000";
									stringItems += item;
								});
								dataPost += stringItems;
								var dadosVendedor = "&reference=REF1234&senderName=Nara Correia&senderAreaCode=81&senderPhone=982345039&senderEmail=contato@narajewelry.com.br"
								dataPost += dadosVendedor;
								var dadosPostais = $('#dadosPostais').serialize();
								dataPost += "&shippingType=1&"+dadosPostais+"&shippingAddressCountry=BRA";
								console.log(dataPost);

								$.ajax({
									// url: curl,
									url: "<?php bloginfo( 'template_url' ); ?>/pagSeguro.php",
									type: "POST",
									// headers: {  'Access-Control-Allow-Origin': 'https://ws.pagseguro.uol.com.br' },
									// crossDomain: true,
									// dataType: 'jsonp',
									contentType: "application/x-www-form-urlencoded",
									data: dataPost,
									complete: function(data) {
										console.log(data);
									},
									success: function(data) {
										console.log(data);
									},
									error: function(data) {
										console.log(data);
									}	
								});
							}
						</script>

						<hr>
						
						<h2>Preencha seus dados:</h2>
						<form name="dadosPostais" id="dadosPostais" action="<?php bloginfo( 'template_url' ); ?>/pagseguro/checkout.php" method="post">
						<div class="col-md-6">
							<input type="text" name="nome" value="Nome completo" onFocus="if (this.value == 'Nome completo') this.value = '';" onBlur="if (this.value == '') this.value = 'Nome completo';" style="width: 100%; float: left;" required/>
							<input type="text" name="shippingAddressPostalCode" id="shippingAddressPostalCode" value="Cep" onFocus="if (this.value == 'Cep') this.value = '';" onBlur="if (this.value == '') this.value = 'Cep';" style="width: 100%; float: left;" required/>
							<input type="text" name="shippingAddressNumber" id="shippingAddressNumber" value="Número" onFocus="if (this.value == 'Número') this.value = '';" onBlur="if (this.value == '') this.value = 'Número';" style="width: 100%; float: left;" required/>
							<input type="text" name="shippingAddressDistrict" id="shippingAddressDistrict" value="Bairro" onFocus="if (this.value == 'Bairro') this.value = '';" onBlur="if (this.value == '') this.value = 'Bairro';" style="width: 100%; float: left;" required/>
							<select name="shippingAddressState" id="shippingAddressState" required>
								<option value="">Estado</option>
								<option value="AC">Acre</option>
								<option value="AL">Alagoas</option>
								<option value="AP">Amapá</option>
								<option value="AM">Amazonas</option>
								<option value="BA">Bahia</option>
								<option value="CE">Ceará</option>
								<option value="DF">Distrito Federal</option>
								<option value="ES">Espírito Santo</option>
								<option value="GO">Goiás</option>
								<option value="MA">Maranhão</option>
								<option value="MT">Mato Grosso</option>
								<option value="MS">Mato Grosso do Sul</option>
								<option value="MG">Minas Gerais</option>
								<option value="PA">Pará</option>
								<option value="PB">Paraíba</option>
								<option value="PR">Paraná</option>
								<option value="PE">Pernambuco</option>
								<option value="PI">Piauí</option>
								<option value="RJ">Rio de Janeiro</option>
								<option value="RN">Rio Grande do Norte</option>
								<option value="RS">Rio Grande do Sul</option>
								<option value="RO">Rondônia</option>
								<option value="RR">Roraima</option>
								<option value="SC">Santa Catarina</option>
								<option value="SP">São Paulo</option>
								<option value="SE">Sergipe</option>
								<option value="TO">Tocantins</option>
							</select>
						</div>

						<div class="col-md-6">
							<input type="text" name="email" value="Email" onFocus="if (this.value == 'Email') this.value = '';" onBlur="if (this.value == '') this.value = 'Email';" style="width: 100%; float: left;" required/>
							<input type="text" name="shippingAddressStreet" id="shippingAddressStreet" value="Endereço" onFocus="if (this.value == 'Endereço') this.value = '';" onBlur="if (this.value == '') this.value = 'Endereço';" style="width: 100%; float: left;" required/>
							<input type="text" name="shippingAddressComplement" id="shippingAddressComplement" value="Complemento" onFocus="if (this.value == 'Complemento') this.value = '';" onBlur="if (this.value == '') this.value = 'Complemento';" style="width: 100%; float: left;" required/>
							<input type="text" name="shippingAddressCity" id="shippingAddressCity" value="Cidade" onFocus="if (this.value == 'Cidade') this.value = '';" onBlur="if (this.value == '') this.value = 'Cidade';" style="width: 100%; float: left;" required/>
						</div>
						<!-- provisório -->
						<input type="hidden" name="items" id="items"/>
						<div class="col-md-12 text-right" style="padding-top: 50px; display: grid;">
							<button type="submit" style="border: 0; background: none;">
								<img src="<?php bloginfo( 'template_url' ); ?>/images/botao_pagar_pagseguro.jpg" width="250">
							</button>
						</div>
						<!-- / provisório -->
						</form>
						<!-- <div class="col-md-12 text-right">
							<a href="#" style="float: right; margin-top: 72px;" onclick="pagSeguro(event)">
								<img src="<?php bloginfo( 'template_url' ); ?>/images/botao_pagar_pagseguro.jpg" width="250">
							</a>
						</div> -->
					</div><!-- //ROW -->

				</div><!-- //CONTAINER -->
			</div><!-- //SERVICES -->
			
		</section><!-- //ABOUT -->
		
	</div><!-- //PAGE -->

	<script type="text/javascript">
		var cart = JSON.parse(sessionStorage.getItem("carrinho"));
		console.log(cart);
		var stringItems = "";//provisorio
		$.each( cart, function( key, value ) {
			var i = key+1;
			var item = "&itemId"+i+"=000"+i+"&itemDescription"+i+"="+value.itemDescription+"&itemAmount"+i+"="+(value.itemAmount.substr(3)).replace(/,/g, '.')+"&itemQuantity"+i+"="+value.itemQuantity+"&itemWeight"+i+"=1000";
			stringItems += item;
			if(value.tamanho !== undefined) {
				var tamanho = "&itemTamanho"+i+"="+value.tamanho;
				stringItems += tamanho;
			}
			if(value.cor !== undefined) {
				var cor = "&itemCor"+i+"="+value.cor;
				stringItems += cor;
			}
			if(value.frase !== undefined) {
				var frase = "&itemFrase"+i+"="+value.frase;
				stringItems += frase;
			}
			if(value.opcaoFrase !== undefined) {
				var opcaoFrase = "&itemOpcaoFrase"+i+"="+value.opcaoFrase;
				stringItems += opcaoFrase;
			}
			if(value.opcaoCoracao !== undefined) {
				var opcaoCoracao = "&itemOpcaoCoracao"+i+"="+value.opcaoCoracao;
				stringItems += opcaoCoracao;
			}
			if(value.estampa !== undefined) {
				var estampa = "&itemEstampa"+i+"="+value.estampa;
				stringItems += estampa;
			}
			if(value.pedra !== undefined) {
				var pedra = "&itemPedra"+i+"="+value.pedra;
				stringItems += pedra;
			}
			if(value.corFio !== undefined) {
				var corFio = "&itemCorFio"+i+"="+value.corFio;
				stringItems += corFio;
			}
		});
		$('#items').attr('value', stringItems);//provisorio
		console.log($('#items').val())
	</script>

<?php
get_footer();