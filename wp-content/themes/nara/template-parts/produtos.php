<?php
get_header();
?>
	<?php
		$parent_title = get_the_title( $post->post_parent );
		$category = $_GET['group'];
	?>

	<!-- PAGE -->
	<div id="page">

		<!-- ADCIONANDO MENU -->
		<?php
			get_template_part( 'template-parts/menu', 'menu' );
		?>
		<!-- / ADCIONANDO MENU -->

		<!-- BREADCRUMBS -->
		<section class="breadcrumbs_block clearfix parallax">
			<div class="container center">
				<h2><?php print_r($parent_title) ?></h2>
				<p><?php the_field('subtitulo') ?> <b><?php echo $category ?></b></p>
			</div>
		</section><!-- //BREADCRUMBS -->

		<!-- ABOUT -->
		<section id="about">

			<!-- SERVICES -->
			<div class="services_block" data-appear-top-offset="-200" data-animated="fadeInUp">

				<!-- CONTAINER -->
				<div class="container">
					
					<!-- ROW -->
					<div class="row">

						<?php
							$args = array('post_type' => 'product', 'post_per_page' => -1, 'nopaging' => true );
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post();
							if(in_category($category)) {
							$itemId = $post->ID;
						?>
						<div class="col-md-4 prod-cat">
							<a href="<?php echo esc_url( home_url( '/produto?item=' . $itemId ) ); ?>">
								<div class="prod-cat-img">
									<img src="<?php the_field('foto_preview'); ?>" alt="" />
								</div>
								<h2><?php the_title(); ?></h2>
								<p class="price">R$ <?php the_field('preco'); ?></p>
								<hr>
								<input class="contact_btn" type="submit" value="Detalhes do produto" style="width:100%;" />
							</a>
						</div>
						<?php 
							}
							endwhile;
							wp_reset_query();
						?>
						
					</div><!-- //ROW -->
				</div><!-- //CONTAINER -->

			</div><!-- //SERVICES -->
		</section><!-- //ABOUT -->

		<!-- NEWS -->
		<section id="news">

			<!-- CONTAINER -->
			<div class="container">
				<h2><b>Opinião</b> de Clientes</h2>

				<!-- TESTIMONIALS -->
				<div class="testimonials" data-appear-top-offset="-200" data-animated="fadeInUp">

					<!-- TESTIMONIALS SLIDER -->
					<div class="owl-demo owl-carousel testim_slider">

						<!-- TESTIMONIAL1 -->
						<div class="item">
							<div class="testim_content">“Poderia dizer que a Nara tem um talento único. Consegue fazer peças elegantes,
								modernas e quase que exclusivas. Acompanho o trabalho dela desde que ela começou e me apaixono em cada
								lançamento ♥.”</div>
							<div class="testim_author">— Roseanne Alves</div>
						</div><!-- TESTIMONIAL1 -->

						<!-- TESTIMONIAL2 -->
						<div class="item">
							<div class="testim_content">“Nara é uma artista da afetividade, além de ser extremamente criativa, talentosa e
								sensível , usa suas obras , criações, para nos podermos “ gravar “ nossos sentires para sempre !”</div>
							<div class="testim_author">— Pilar Brown</div>
						</div><!-- TESTIMONIAL2 -->

						<!-- TESTIMONIAL3 -->
						<div class="item">
							<div class="testim_content">“O trabalho da Nara é tão sensível e precioso que isso está potencialmente presente
								nas peças dela. Me emociona. Tem 3 anos que eu uso meus anéis todos os dias. Sinto eles sendo tão parte de
								mim, que quando estou sem eles é como se eu não estivesse completa.”</div>
							<div class="testim_author">— Camilla Molica</div>
						</div><!-- TESTIMONIAL3 -->

					</div><!-- TESTIMONIALS SLIDER -->
				</div><!-- //TESTIMONIALS -->


				<div class="row">
					<div class="col-md-6 col-centered">
						<ul class="social">
							<li>SIGA-NOS:</li>
							<li><a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a></li>
							<li><a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a></li>
							<li><a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a></li>
						</ul>
					</div>
				</div>
			</div><!-- //CONTAINER -->
		</section><!-- //NEWS -->
	</div><!-- //PAGE -->

<?php
get_footer();