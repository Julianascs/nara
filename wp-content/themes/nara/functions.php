<?php
	// Register Custom Post Type
	function products_post_type() {

		$labels = array(
			'name'                  => _x( 'Produtos', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Produto', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Produtos', 'text_domain' ),
			'name_admin_bar'        => __( 'Produto', 'text_domain' ),
			'archives'              => __( 'Arquivos de itens', 'text_domain' ),
			'attributes'            => __( 'Atributos do Item', 'text_domain' ),
			'parent_item_colon'     => __( 'Produto pai:', 'text_domain' ),
			'all_items'             => __( 'Todos os Produtos', 'text_domain' ),
			'add_new_item'          => __( 'Adicionar novo produto', 'text_domain' ),
			'add_new'               => __( 'Novo produto', 'text_domain' ),
			'new_item'              => __( 'Novo Item', 'text_domain' ),
			'edit_item'             => __( 'Editar produto', 'text_domain' ),
			'update_item'           => __( 'Atualizar Produto', 'text_domain' ),
			'view_item'             => __( 'Visualizar Produto', 'text_domain' ),
			'view_items'            => __( 'Visualizar Items', 'text_domain' ),
			'search_items'          => __( 'Buscar produtos', 'text_domain' ),
			'not_found'             => __( 'Nenhum produto encontrado', 'text_domain' ),
			'not_found_in_trash'    => __( 'Nenhum produto encontrado na lixeira', 'text_domain' ),
			'featured_image'        => __( 'Imagem em destaque', 'text_domain' ),
			'set_featured_image'    => __( 'Definir imagem em destaque', 'text_domain' ),
			'remove_featured_image' => __( 'Remover imagem em destaque', 'text_domain' ),
			'use_featured_image'    => __( 'Use como imagem em destaque', 'text_domain' ),
			'insert_into_item'      => __( 'Inserir no item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Enviado para este item', 'text_domain' ),
			'items_list'            => __( 'Lista de itens', 'text_domain' ),
			'items_list_navigation' => __( 'Navegação da lista de itens', 'text_domain' ),
			'filter_items_list'     => __( 'Lista de itens de filtro', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Produto', 'text_domain' ),
			'description'           => __( 'Página de informação do produto.', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'custom-fields' ),
			'taxonomies'            => array( 'category', 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
		);
		register_post_type( 'product', $args );

	}
	add_action( 'init', 'products_post_type', 0 );
?>