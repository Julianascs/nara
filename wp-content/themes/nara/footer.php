	<!-- CONTACTS -->
	<section id="contacts">
	</section><!-- //CONTACTS -->
	
	<!-- FOOTER -->
	<footer>
		<div id="footer_bg"></div>
		<!-- CONTAINER -->
		<div class="container" style="text-align: center;color: #FFF;">
			<ul class="menu_footer">
				<li>
					<a href="<?php echo esc_url( home_url( '/garantia' ) ); ?>">Garantia</a>
				</li>
				<li>
					<a href="<?php echo esc_url( home_url( '/prazo-entrega-envio' ) ); ?>">Prazo de Entrega e Envio</a>
				</li>
				<li>
					<a href="<?php echo esc_url( home_url( '/troca-devolucao' ) ); ?>">Trocas e Devoluções</a>
				</li>
				<li>
					<a href="<?php echo esc_url( home_url( '/politica-privacidade' ) ); ?>">Política de Privacidade</a>
				</li>
				<li>
					<a href="<?php echo esc_url( home_url( '/seguranca' ) ); ?>">Segurança</a>
				</li>
			</ul>
			<p style="margin-top: 20px;">Nara Jewelry 2019 &copy; Todos os direitos reservados &nbsp;|&nbsp; <b>Contato:</b> nara@narajewelry.com.br</p>
		</div><!-- //CONTAINER -->
	</footer><!-- //FOOTER -->

</div>
</body>
</html>
