<?php
header("access-control-allow-origin: https://pagseguro.uol.com.br");
header("Content-Type: text/html; charset=UTF-8",true);
date_default_timezone_set('America/Sao_Paulo');

require_once("PagSeguro.class.php");
$PagSeguro = new PagSeguro();

require_once("Email.php");
$EmailClass = new EnviaEmail();


//ENVIANDO EMAIL
$dadosPost = $_POST;
$EmailClass->sendEmail($dadosPost);

//LENDO DADOS DO FORMULÁRIO
$venda = array("codigo"=>"1", "codigo_pagseguro"=>"");
$itemsCarrinho = $_POST["items"];
$arrayItems = explode('&', $itemsCarrinho);

foreach($arrayItems as $valores) {
	list($param_name, $param_val) = explode("=", $valores);
	// echo '<pre>';
	// print_r($param_name);
	// echo '</pre>';
	// echo '<pre>';
	// print_r($param_val);
	// echo '</pre>';
	if(strpos($param_name, 'itemId') !== false) {
		$venda[$param_name] = $param_val;
	}
	if(strpos($param_name, 'itemDescription') !== false) {
		$venda[$param_name] = $param_val;
	}
	if(strpos($param_name, 'itemAmount') !== false) {
		$venda[$param_name] = $param_val;
	}
	if(strpos($param_name, 'itemQuantity') !== false) {
		$venda[$param_name] = $param_val;
	}
}
$venda['senderName'] = $_POST["nome"];
$venda['senderEmail'] = $_POST["email"];
$venda['shippingAddressStreet'] = $_POST["shippingAddressStreet"];
$venda['shippingAddressNumber'] = $_POST["shippingAddressNumber"];
$venda['shippingAddressComplement'] = $_POST["shippingAddressComplement"] == "Complemento" ? '' : $_POST["shippingAddressComplement"];
$venda['shippingAddressDistrict'] = $_POST["shippingAddressDistrict"];

//Tratar CEP
$cep = implode("",explode("-",$_POST['shippingAddressPostalCode']));
$cep = implode("",explode(".",$cep));

$venda['shippingAddressPostalCode'] = $cep;
$venda['shippingAddressCity'] = $_POST["shippingAddressCity"];
$venda['shippingAddressState'] = strtoupper($_POST["shippingAddressState"]);

// print_r($venda);
	
//EFETUAR PAGAMENTO	
// $venda = array("codigo"=>"1",
// 			   "valor"=>100.00,
// 			   "descricao"=>"Brinco Antena",
// 			   "nome"=>"Juliana Souza",
// 			   "email"=>"juhcavalcanti.souza@gmail.com",
// 			   "telefone"=>"(81) 9729-1482",
// 			   "rua"=>"Rua francisco da cunha",
// 			   "numero"=>"657",
// 			   "bairro"=>"boa viagem",
// 			   "cidade"=>"Recife",
// 			   "estado"=>"PE", //2 LETRAS MAIÚSCULAS
// 			   "cep"=>"51020041",
// 			   "codigo_pagseguro"=>"");
		
$PagSeguro->executeCheckout($venda,"http://www.narajewelry.com.br/");

// $PagSeguro->executeCheckout($venda,"http://localhost:8080/nara/pedido/".$_GET['codigo']);

//----------------------------------------------------------------------------


//RECEBER RETORNO
if( isset($_GET['transaction_id']) ){
	$pagamento = $PagSeguro->getStatusByReference($_GET['codigo']);
	
	$pagamento->codigo_pagseguro = $_GET['transaction_id'];
	if($pagamento->status==3 || $pagamento->status==4){
		//ATUALIZAR DADOS DA VENDA, COMO DATA DO PAGAMENTO E STATUS DO PAGAMENTO
		
	}else{
		//ATUALIZAR NA BASE DE DADOS
	}
}

?>