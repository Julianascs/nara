<?php
header("access-control-allow-origin: https://ws.pagseguro.uol.com.br");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT');
get_header();
?>

<!-- PAGE -->
<div id="page-home">

	<!-- ADCIONANDO MENU -->
	<?php
		get_template_part( 'template-parts/menu', 'menu' );
	?>
	<!-- / ADCIONANDO MENU -->

	<!-- HOME -->
	<section id="home" class="padbot0">
			
		<!-- TOP SLIDER -->
		<div class="flexslider top_slider">
			<ul class="slides">
				<li class="slide1">
					<div class="flex_caption1">
						<p class="title1 captionDelay2 FromTop">Argolinha </p>
						<p class="title2 captionDelay4 FromTop">Figa</p>
						<!-- <p class="title3 captionDelay6 FromTop">Templates</p> -->
						<p class="title4 captionDelay7 FromBottom">Proteção pra vida! Argolinha, corrente e uma figa para proteção em um brinco cheio de estilo para ser usado em várias ocasiões e fazer várias misturas.</p>
					</div>
					<a class="slide_btn FromRight" href="<?php echo esc_url( home_url( '/produto/?item=104' ) ); ?>" >Ver mais</a>
				<li class="slide2">
					<div class="flex_caption1">
						<p class="title1 captionDelay6 FromLeft">Bracelete</p>
						<p class="title2 captionDelay4 FromLeft">Custom</p>
						<p class="title3 captionDelay2 FromLeft">Slim</p>
						<p class="title4 captionDelay7 FromLeft">O trio de bracelete “AlwaysPositive Vibes” é aquela peça tatuagem, vontade de usar pra sempre. Essa é a versão mais fina do bracelete e pode ser feita com a palavra que você quiser. Clica aqui para encomendar a sua Now!!</p>
					</div>
					<a class="slide_btn FromRight" href="<?php echo esc_url( home_url( '/produto/?item=146' ) ); ?>" >Ver mais</a>
				</li>
				<li class="slide3">
					<div class="flex_caption1">
						<p class="title1 captionDelay1 FromBottom">Choker</p>
						<p class="title2 captionDelay2 FromBottom">Stone </p>
						<!-- <p class="title3 captionDelay3 FromBottom">Natural</p> -->
						<p class="title4 captionDelay5 FromBottom">A Choker Pedra Natural é uma peça que você pode compor com vários estilos e outros acessórios. E ainda escolher a pedra que mais combine com você. Essa é um ônix, a pedra da proteção energética.</p>
					</div>
					<a class="slide_btn FromRight" href="<?php echo esc_url( home_url( '/produto/?item=65' ) ); ?>" >Ver mais</a>
					
					<!-- VIDEO BACKGROUND 
					<a id="P2" class="player" data-property="{videoURL:'tDvBwPzJ7dY',containment:'.top_slider .slide3',autoPlay:true, mute:true, startAt:0, opacity:1}" ></a>-->
					<!-- //VIDEO BACKGROUND -->
				</li>
			</ul>
		</div>
	</section><!-- //HOME -->
	
	
	<!-- ABOUT -->
	<section id="about">
		
		<!-- SERVICES -->
		<div class="services_block padbot40" data-appear-top-offset="-200" data-animated="fadeInUp">
			
			<!-- CONTAINER -->
			<div class="container">
			
				<!-- ROW -->
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-ss-12 margbot30">
						<a class="services_item" href="javascript:void(0);" >
							<p><b>Estilo</b> versus <b>Moda</b></p>
							<span style="padding-right: 50px;">Estilo é diferente de moda: vem de dentro para fora. A moda passa e o estilo não. O caminho para encontrar o seu próprio estilo é conhecer e aprender a combinar a sua personalidade com o seu visual. ps: um estilo não se compra como a moda, é adquirido através dos anos.</span>
						</a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-ss-12 margbot30">
						<a class="services_item" href="javascript:void(0);" >
							<p><b>Tendência</b> na <b>moda</b></p>
							<span>A todo momento somos impactados por várias informações, que logo mais virão tendências. Isso por que? Porque, inevitavelmente, elas ficam guardadas no seu inconsciente (HD). E “de repente” nos pegamos fazendo afirmações do tipo: “Não sei por que estou gostando de usar esse tênis, essa sombra...”. O que devemos aprender é que não devemos só exibir a tendência, e sim sentir-se representados pelas suas escolhas.</span>
						</a>
					</div>
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</div><!-- //SERVICES -->
		
		<!-- CLEAN CODE -->
		<div class="cleancode_block">
			
			<!-- CONTAINER -->
			<div class="container" data-appear-top-offset="-200" data-animated="fadeInUp">
				
				<!-- CASTOM TAB -->
				<div id="myTabContent" class="tab-content">
					<div class="tab-pane fade in active clearfix" id="tab1">
						<p class="title"><b>Design</b> Personalizado</p>
						<span>Aqui vocês irão ficar com um gostinho de quero mais. Lembrar que sempre vai ter uma história pra contar em formato de joia.
						Com palavras, frases, lugares, pessoas.... e pedra. Alguma coisa que marcaram vocês em algum momento da vida.</span>
					</div>
					<div class="tab-pane fade clearfix" id="tab2">
						<p class="title"><b>Brincos</b></p>
						<span>Jóias artesanais feitas especialmente para você </span> &nbsp;<a href="<?php echo esc_url( home_url( '/produtos?group=brinco' ) ); ?>">clique aqui →</a>
					</div>
					<div class="tab-pane fade clearfix" id="tab3">
						<p class="title"><b>Colares</b></p>
						<span>Jóias artesanais feitas especialmente para você </span> &nbsp;<a href="<?php echo esc_url( home_url( '/produtos?group=colar' ) ); ?>">clique aqui →</a>
					</div>
					<div class="tab-pane fade clearfix" id="tab4">
						<p class="title"><b>Chokers</b></p>
						<span>Jóias artesanais feitas especialmente para você </span> &nbsp;<a href="<?php echo esc_url( home_url( '/produtos?group=chocker' ) ); ?>">clique aqui →</a>
					</div>
					<div class="tab-pane fade clearfix" id="tab5">
						<p class="title"><b>Anéis</b></p>
						<span>Jóias artesanais feitas especialmente para você </span> &nbsp;<a href="<?php echo esc_url( home_url( '/produtos?group=anel' ) ); ?>">clique aqui →</a>
					</div>
					<div class="tab-pane fade clearfix" id="tab6">
						<p class="title"><b>Pulseiras</b></p>
						<span>Jóias artesanais feitas especialmente para você </span> &nbsp;<a href="<?php echo esc_url( home_url( '/produtos?group=pulseira' ) ); ?>">clique aqui →</a>
					</div>
				</div>
				<ul id="myTab" class="nav nav-tabs">
					<li class="active"><a class="i1" href="#tab1" data-toggle="tab" ><i></i><span>Design Personalizado</span></a></li>
					<li><a class="i2" href="#tab2" data-toggle="tab" ><i></i><span>Brincos</span></a></li>
					<li><a class="i3" href="#tab3" data-toggle="tab" ><i></i><span>Colares</span></a></li>
					<li><a class="i4" href="#tab4" data-toggle="tab" ><i></i><span>Chokers</span></a></li>
					<li><a class="i5" href="#tab5" data-toggle="tab" ><i></i><span>Anéis</span></a></li>
					<li><a class="i6" href="#tab6" data-toggle="tab" ><i></i><span>Pulseiras</span></a></li>
				</ul><!-- CASTOM TAB -->
			</div><!-- //CONTAINER -->
		</div><!-- //CLEAN CODE -->
		
		<!-- MULTI PURPOSE -->
		<div class="purpose_block">
			
			<!-- CONTAINER -->
			<div class="container">
				
				<!-- ROW -->
				<div class="row">
				
					<div class="col-lg-7 col-md-7 col-sm-7" data-appear-top-offset="-200" data-animated="fadeInLeft">
						<h2>Pulseira <b>Lenço</b></h2>
						<p>A Pulseira Lenço é mais um dos nossos produtos com a intenção do personalizado. Aqui você pode escolher sua frase ou palavra e ainda a cor do seu lenço, o que combine mais com seu estilo e personalidade.</p>

						<p>Alias, a Pulseira Lenço já remete uma personalidade no seu look. Então vamos ousar!!</p>
						<a class="btn btn-active" href="<?php echo esc_url( home_url( '/produto/?item=51' ) ); ?>"><span data-hover="Comprar">Comprar</span></a>
						<a class="btn" href="<?php echo esc_url( home_url( '/produto/?item=51' ) ); ?>" >Ver mais detalhes</a>
					</div>
					
					<div class="col-lg-5 col-md-5 col-sm-5 ipad_img_in" data-appear-top-offset="-200" data-animated="fadeInRight">
						<img class="ipad_img1" src="<?php bloginfo( 'template_url' ); ?>/images/img1.png" alt="" />
					</div>
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</div><!-- //MULTI PURPOSE -->
	</section><!-- //ABOUT -->
	
	
	<!-- PROJECTS -->
	<section id="projects" class="padbot20" style="display: none;">
	
		<!-- OUR CLIENTS -->
		<div class="our_clients">
		
			<!-- CONTAINER -->
			<div class="container" data-appear-top-offset="-200" data-animated="fadeInUp">
				
				<!-- ROW -->
				<div class="row" style="display: none;">
					<div class="col-lg-2 col-md-2 col-sm-2 client_img">
						<img src="<?php bloginfo( 'template_url' ); ?>/images/clients/1.jpg" alt="" />
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 client_img">
						<img src="<?php bloginfo( 'template_url' ); ?>/images/clients/2.jpg" alt="" />
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 client_img">
						<img src="<?php bloginfo( 'template_url' ); ?>/images/clients/3.jpg" alt="" />
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 client_img">
						<img src="<?php bloginfo( 'template_url' ); ?>/images/clients/4.jpg" alt="" />
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 client_img">
						<img src="<?php bloginfo( 'template_url' ); ?>/images/clients/5.jpg" alt="" />
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 client_img">
						<img src="<?php bloginfo( 'template_url' ); ?>/images/clients/6.jpg" alt="" />
					</div>
				</div><!-- //ROW -->
			</div><!-- CONTAINER -->
		</div><!-- //OUR CLIENTS -->
	</section><!-- //PROJECTS -->
	
	
	<!-- TEAM -->
	<section id="team">
	
		<!-- CONTAINER -->
		<div class="container">
			<h2>Os mais <b>vendidos</b></h2>
			
			<!-- ROW -->
			<div class="row" data-appear-top-offset="-200" data-animated="fadeInUp">
					
				<!-- TEAM SLIDER -->
				<div class="owl-demo owl-carousel team_slider">
					<?php 
						$args = array( 'post_type' => 'product', 'post_per_page' => -1, 'nopaging' => true );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post();
						if(has_tag('destaque_home')) {
							$itemId = $post->ID
					?>
					<!-- crewman1 -->
					<div class="item">
						<div class="crewman_item">
							<div class="crewman">
								<img src="<?php the_field('foto_destaque_home'); ?>" alt="" />
							</div>
							<div class="crewman_descr center">
								<div class="crewman_descr_cont">
									<p><?php the_title(); ?></p>
								</div>
							</div>
							<div class="crewman_social">
								<a href="<?php echo esc_url( home_url( '/produto?item=' . $itemId ) ); ?>">ver mais</a>
							</div>
						</div>
					</div>
					<!-- crewman1 -->
					<?php
						}
						endwhile;
						wp_reset_query();
					?>
				</div><!-- TEAM SLIDER -->
			</div><!-- //ROW -->
		</div><!-- //CONTAINER -->
	</section><!-- //TEAM -->
	
	
	<!-- NEWS -->
	<section id="news">
	
		<!-- CONTAINER -->
		<div class="container">
			<h2><b>Opinião</b> de Clientes</h2>
			
			<!-- TESTIMONIALS -->
			<div class="testimonials" data-appear-top-offset="-200" data-animated="fadeInUp">
					
				<!-- TESTIMONIALS SLIDER -->
				<div class="owl-demo owl-carousel testim_slider">
					
					<!-- TESTIMONIAL1 -->
					<div class="item">
						<div class="testim_content">“Poderia dizer que a Nara tem um talento único. Consegue fazer peças elegantes, modernas e quase que exclusivas. Acompanho o trabalho dela desde que ela começou e me apaixono em cada lançamento ♥.”</div>
						<div class="testim_author">—  Roseanne Alves</div>
					</div><!-- TESTIMONIAL1 -->
					
					<!-- TESTIMONIAL2 -->
					<div class="item">
						<div class="testim_content">“Nara é uma artista da afetividade, além de ser extremamente criativa, talentosa e sensível , usa suas obras , criações, para nos podermos “ gravar “ nossos sentires para sempre !”</div>
						<div class="testim_author">—  Pilar Brown</div>
					</div><!-- TESTIMONIAL2 -->

					<!-- TESTIMONIAL3 -->
					<div class="item">
						<div class="testim_content">“O trabalho da Nara é tão sensível e precioso que isso está potencialmente presente nas peças dela. Me emociona. Tem 3 anos que eu uso meus anéis todos os dias. Sinto eles sendo tão parte de mim, que quando estou sem eles é como se eu não estivesse completa.”</div>
						<div class="testim_author">—  Camilla Molica</div>
					</div><!-- TESTIMONIAL3 -->

				</div><!-- TESTIMONIALS SLIDER -->
			</div><!-- //TESTIMONIALS -->
			
			<!-- RECENT POSTS -->
			<div class="row recent_posts" data-appear-top-offset="-200" data-animated="fadeInUp">
				<h2>No <b>Instagram</b></h2>
				<!-- LightWidget WIDGET -->
				<script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script>
				<iframe src="//lightwidget.com/widgets/2be0b64a847e5d0682cbc5ac5f769f1e.html" scrolling="no" 
				allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>
				 <!-- <div class="col-lg-3 col-md-3 col-sm-3 padbot30 post_item_block">
					<div class="post_item">
						<div class="post_item_img">
							<img src="<?php bloginfo( 'template_url' ); ?>/images/blog/1.jpg" alt="" />
							<a class="link" href="https://www.instagram.com/p/BqTpwAijx46/" target="_blank"></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 padbot30 post_item_block">
					<div class="post_item">
						<div class="post_item_img">
							<img src="<?php bloginfo( 'template_url' ); ?>/images/blog/2.jpg" alt="" />
							<a class="link" href="https://www.instagram.com/p/BqNux4cjevZ/" target="_blank"></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 padbot30 post_item_block">
					<div class="post_item">
						<div class="post_item_img">
							<img src="<?php bloginfo( 'template_url' ); ?>/images/blog/3.jpg" alt="" />
							<a class="link" href="https://www.instagram.com/p/Bp7zT8CD4Ls/" target="_blank"></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 padbot30 post_item_block">
					<div class="post_item">
						<div class="post_item_img">
							<img src="<?php bloginfo( 'template_url' ); ?>/images/blog/4.jpg" alt="" />
							<a class="link" href="https://www.instagram.com/p/Bpvd9K-jpuI/" target="_blank"></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 padbot30 post_item_block">
					<div class="post_item">
						<div class="post_item_img">
							<img src="<?php bloginfo( 'template_url' ); ?>/images/blog/5.jpg" alt="" />
							<a class="link" href="https://www.instagram.com/p/BpiAZGXDspM/" target="_blank"></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 padbot30 post_item_block">
					<div class="post_item">
						<div class="post_item_img">
							<img src="<?php bloginfo( 'template_url' ); ?>/images/blog/6.jpg" alt="" />
							<a class="link" href="https://www.instagram.com/p/BpCrtWgDWwE/" target="_blank"></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 padbot30 post_item_block">
					<div class="post_item">
						<div class="post_item_img">
							<img src="<?php bloginfo( 'template_url' ); ?>/images/blog/7.jpg" alt="" />
							<a class="link" href="https://www.instagram.com/p/Brm-1DWDUnX/" target="_blank"></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 padbot30 post_item_block">
					<div class="post_item">
						<div class="post_item_img">
							<img src="<?php bloginfo( 'template_url' ); ?>/images/blog/8.jpg" alt="" />
							<a class="link" href="https://www.instagram.com/p/BrdlbDBDyt0/" target="_blank"></a>
						</div>
					</div>
				</div> -->
				<!-- <script src="https://apps.elfsight.com/p/platform.js" defer></script>
				<div class="elfsight-app-47e64bd4-0cf5-4b3b-981e-9ac4e23930f3"></div> -->
				
			</div>
			<!-- RECENT POSTS -->

			<div class="row">
				<div class="col-md-6 col-centered">
					<ul class="social">
						<li>SIGA-NOS:</li>
						<li><a href="https://www.instagram.com/_narajewelry/?hl=pt-br" target="_blank"><i class="fa fa-instagram"></i></a></li>
						<li><a href="https://www.facebook.com/naracarolina" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://br.pinterest.com/naraccorrea/nara-jewelry/" target="_blank"><i class="fa fa-pinterest-square"></i></a></li>
					</ul>
				</div>
			</div>
		</div><!-- //CONTAINER -->
	</section><!-- //NEWS -->
</div><!-- //PAGE -->

<?php
get_footer();