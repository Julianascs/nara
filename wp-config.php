<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'narajewecom_nara');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');
// define('DB_USER', 'narajewecom_admin');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');
// define('DB_PASSWORD', 'N@r@1123581*');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');
// define('DB_HOST', 'localhost:3306');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ')cv^_=qNH&CR<efmvko<H8w}0Mi{Y}} 6of?FxhDU$HX&<@L+tIMKC?c[x5R=!]V');
define('SECURE_AUTH_KEY',  'Q.Hp;4o!2vs!$lcjMnG1BjSTimwS,qah;`()Y@&cuUT,3Mx4W#s$Am}RPl4qQYVD');
define('LOGGED_IN_KEY',    '6sKS/i(|Dp%!&hhOYdS5?YL)`q%Z>ct*o|=W]@:>(K`hWqrdv<d7T-36z&Hm[xNt');
define('NONCE_KEY',        'e1r}w8JM?(cl#:6nfqYCG%7PXu{e!.d-QK7=VUHyW4+%{7E!*JdXA:%k.?y+-0f^');
define('AUTH_SALT',        'fzT-8K[45Eibz$-R:D%cw ]FrHVCx}?^WXMtthBCX~uc[]|Y890r0p@L5NW((kWj');
define('SECURE_AUTH_SALT', '8.8k@VIoV!]F&:c>,&NQ):d.?$!Sj|O,JQ&cT5dK9j?U@+QA~/ct_|EM`YQv?.&W');
define('LOGGED_IN_SALT',   'Gl9Lz!dY>CpFuG+sfjZau=C;HQY3[o7K{qCJ*6+pE4<4a;_LHF&^_z0I=ZwM!J&(');
define('NONCE_SALT',       '~}W{sBH1pn=pbh{._.z,G0RQ`xvv$!N@^hYNaaM]<u=(OQo(93KH :gUn]1sRO0Q');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'tb_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
